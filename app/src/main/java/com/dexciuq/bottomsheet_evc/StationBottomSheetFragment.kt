package com.dexciuq.bottomsheet_evc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.dexciuq.bottomsheet_evc.databinding.FragmentStationBottomSheetBinding
import com.dexciuq.bottomsheet_evc.model.Station
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class StationBottomSheetFragment(
    private val station: Station,
    private val onOpenInGoogleMapClick: (Station) -> Unit = {},
) : BottomSheetDialogFragment() {

    private val binding by lazy { FragmentStationBottomSheetBinding.inflate(layoutInflater) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return with(binding) {
            stationImageView.setImageResource(station.image)
            name.text = station.name
            city.text = station.city
            location.text = station.location

            availablePorts.text = getString(R.string.available_port_count, station.availablePorts)
            if (station.id == 2L) availablePorts.setTextColor(
                ContextCompat.getColor(requireContext(), R.color.yellow)
            )

            openInGoogleMapsButton.setOnClickListener {
                onOpenInGoogleMapClick(station)
                dismiss()
            }

            root
        }
    }
}