package com.dexciuq.bottomsheet_evc.model

data class Station(
    val id: Long,
    val image: Int,
    val name: String,
    val location: String,
    val city: String,
    val availablePorts: Int,
)
